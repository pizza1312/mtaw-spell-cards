#!/usr/bin/env python3

    
import PyPDF2
from dicttoxml import dicttoxml

import collections
collections.Iterable = collections.abc.Iterable
#print("Hello world!")

ranks = ["Initiate", "Apprentice", "Disciple", "Adept", "Master"]
ranks_with_dots = ["•Initiate", "••Apprentice", "•••Disciple", "••••Adept", "•••••Master"]
arcana = ["Death", "Fate", "Forces", "Life", "Matter", "Mind", "Prime", "Space", "Spirit", "Time"]

def get_dots(rank):
    dot_count = 1
    for r in ranks:
        if r == rank:
            return "•"*dot_count
        dot_count += 1
    print("Warning: no dots found for rank",rank)
    return ""


def replace_char_at(s, char, i):
    out = s[:i] + char + s[i+1:]
    return out

def strip_double_spaces(s):
    out = s
    while "  " in out:
        out = out.replace("  ", "")
    return out

def replace_strings(s):
    rpl = {
        "Pr actice" : "Practice",
        ") ." : ").",
        " ," : ",",
        "FrayingorPerfecting" : "Fraying or Perfecting",
        "Inf luence" : "Influence",
    }

    out = s
    for key, val in rpl.items():
        out = out.replace(key, val)
    return out

def fix_punctuation(s):
    out = s
    for i,c in enumerate(out):
        if c == ":":
            if s[i+1] != " " and s[i-1] == " ":
                c = "€" # Euro sign means double wrong spacing. " €" replace with ": "
                out = replace_char_at(out, "€", i)
            elif s[i-1] == " ":
                c = "£" # Pound sign means extra space before. " £" replace with ":"
                out = replace_char_at(out, "£", i)
            elif s[i+1] != " ":
                c = "@" # Ampersand means no space after. "@" replace with ": "
                out = replace_char_at(out, "@", i)

    out = out.replace("@",": ").replace(" €", ": ").replace(" £", ":")
    return out

def fix_wordbreak(s):
    return s.replace(" -\n","").replace("-\n", "")

def fix_rank_dot_linebreak(s):
    out = s
    for rank in ranks_with_dots:
        out = out.replace(rank, "\n"+rank)
    return out

def extract():

    pdf = PyPDF2.PdfFileReader("spellsonly.pdf")

    #print(pdf.numPages)

    spells = {}
    for arcanum in arcana:
        spells[arcanum] = {}
        for rank in ranks:
            spells[arcanum][rank] = []

    current_arcanum = ""
    current_rank = ""
    current_imago = ""

    imago_content = ""
    new_imago = False

    current_imago = current_arcanum = current_rank = practice = psf = srs = wst = cst = ""

    for page in pdf.pages:
        #print("-----Page------")
        text = page.extractText()
        fixed = fix_rank_dot_linebreak(fix_wordbreak(fix_punctuation(replace_strings(strip_double_spaces(text)))))
        # Ok, now remove weird headers at the top of the page
        lines = fixed.split("\n")
        lines = lines[1:]
        if "chapter four" in lines[0]:
            lines = lines[1:]

        for line in lines:

            if len(line)>0 and line[0] == "•" and any(rank in line for rank in ranks):
                if len(imago_content.strip()) > 0 and len(current_arcanum.strip()) > 0 and len(current_imago.strip()) > 0:
                    spell = {
                        "name" : current_imago,
                        "arcanum" : current_arcanum,
                        "rank" : current_rank,
                        "practice" : practice,
                        "primary_factor" : psf,
                        "suggested_rote_skills" : srs,
                        "description" : imago_content.strip(),
                        "dots" : get_dots(current_rank),

                    }

                    if len(wst) > 0:
                        spell["withstand"] = wst
                    if len(cst) > 0:
                        spell["cost"] = cst

                    
                    spells[current_arcanum][current_rank] += [spell]

                    practice = psf = srs = wst = cst = ""

                    #print("CONTENTr:", imago_content)
                    imago_content = ""
                    #print("\n")

                #print("RANK:",line.replace("•", ""))
                current_arcanum = line.split("of ")[-1]
                current_rank = line.split("•")[-1].strip().split(" ")[0]
                category = current_rank + " of " + current_arcanum

            else:

                if len(line)>0 and line[0] == " " and "•" in line and line[0] != "(" and "p. 1" not in line:
                    new_imago_name = line[1:].split(" (")[0]
                    new_imago = True
                elif "•" in line and "p. 1" not in line and any("("+arcanum in line for arcanum in arcana):
                    new_imago_name = line.split(" (")[0]
                    new_imago = True

                if new_imago:
                    if len(imago_content.strip()) > 0 and len(current_arcanum.strip()) > 0 and len(current_imago.strip()) > 0:
                        spell = {
                            "name" : current_imago,
                            "arcanum" : current_arcanum,
                            "rank" : current_rank,
                            "practice" : practice,
                            "primary_factor" : psf,
                            "suggested_rote_skills" : srs,
                            "description" : imago_content.strip(),
                            "dots" : get_dots(current_rank),

                        }

                        if len(wst) > 0:
                            spell["withstand"] = wst
                        if len(cst) > 0:
                            spell["cost"] = cst


                        spells[current_arcanum][current_rank] += [spell]

                        practice = psf = srs = wst = cst = ""

                        #print("CONTENTr:", imago_content)
                        imago_content = ""
                        #print("\n")
                        #print("IMAGO:",current_imago)

                    new_imago = False
                    current_imago = new_imago_name


                else:
                    # just get content
                    if "Practice: " in line:
                        practice = line.split("Practice: ")[1].strip()
                        #print("--PRACTICE",practice)
                    elif "Factor: " in line and "Suggested Rote Skills: " in line:
                        psf = line.split("Factor: ")[1].split("Suggested Rote")[0].strip()
                        srs = line.split("Suggested Rote Skills: ")[1].strip()
                    elif "Primary Spell Factor: " in line:
                        psf = line.split("Primary Spell Factor: ")[1].strip()
                        #print("--PSF",psf)
                    elif "Primary Factor: " in line:
                        psf = line.split("Primary Factor: ")[1].strip()
                        #print("--PSF",psf)
                    elif "Suggested Rote Skills: " in line:
                        srs = line.split("Suggested Rote Skills: ")[1].strip()
                        #print("--SRS",srs)
                    elif "Cost: " in line and "Mana" in line:
                        cost = line.split("Cost: ")[1].strip()
                        #print("--CST", cost)
                    elif "Withstand: " in line:
                        wst = line.split("Withstand: ")[1].strip()
                        #print("--WST", wst)
                    else:
                        imago_content += line.replace(".", ". ").replace("  ", " ").strip()+" "

    if len(imago_content.strip()) > 0 and len(current_arcanum.strip()) > 0 and len(current_imago.strip()) > 0:
        spell = {
            "name" : current_imago,
            "arcanum" : current_arcanum,
            "rank" : current_rank,
            "practice" : practice,
            "primary_factor" : psf,
            "suggested_rote_skills" : srs,
            "description" : imago_content.strip(),
            "dots" : get_dots(current_rank),
        }

        if len(wst) > 0:
            spell["withstand"] = wst
        if len(cst) > 0:
            spell["cost"] = cst

        
        spells[current_arcanum][current_rank] += [spell]

    spells_bulk = []
    for arcanum in arcana:
        for rank in ranks:
            #for spell in spells[arcanum][rank]:
                #print(spell["name"].upper(), "("+spell["rank"], "of", spell["arcanum"]+")", "PRA:",spell["practice"])
            spells_bulk += spells[arcanum][rank]

    xml = dicttoxml(spells, custom_root = "spells", attr_type = False)

    with open("spells.xml", "wb") as f:
        f.write(xml)

    xml2 = dicttoxml(spells_bulk, custom_root = "spells", attr_type = False)

    with open("spells_bulk.xml", "wb") as f:
        f.write(xml2)

    return spells

if __name__ == "__main__":
    extract()

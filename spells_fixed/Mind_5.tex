\Spell{Amorality}{Mind $\bullet$$\bullet$$\bullet$$\bullet$$\bullet$}{Unmaking}{Duration}{}{Resolve}{Crafts, Empathy, Expression}%
{The mage severs her subject’s ties to his guiding impulses, either completely removing his Virtue or his Vice. While without a Virtue, the subject is more prone to indulging his Vice and gains two points of Willpower whenever he would normally gain one. While without a Vice, the character acts in a manner completely consistent with his Virtue, and is incapable of actively engaging in activities that would constitute a breaking point or Act of Hubris. Witnessing heinous or horrifying deeds still causes breaking points for Sleeper characters.}

\Spell{No Exit}{Mind $\bullet$$\bullet$$\bullet$$\bullet$$\bullet$}{Making}{Duration}{}{Resolve}{Expression, Persuasion, Science}%
{The mage creates a mental thought loop for her subject, trapping him within his own mind. For the Duration of the spell, the subject is unable to do anything but play through a single continuous loop in his mind. Thoughts cannot enter or exit the subject’s mind, and he appears nearly catatonic to outside observers. Attempts to read the subject’s mind or memories reveal the thought loop. Supernatural attempts to force new thoughts provoke a Clash of Wills.}

\Spell{Mind Wipe}{Mind $\bullet$$\bullet$$\bullet$$\bullet$$\bullet$}{Unmaking}{Potency}{}{Resolve}{Academics, Intimidation, Occult}%
{The mage removes a large portion of the subject’s memories. The victim suffers from the Amnesia Condition for the Duration of the spell, unable to recall one month of time per level of Potency. The mage may specify which portion of the subject’s life is forgotten.\\ \textbf{+1 Reach:} The mage may choose the memories erased rather than wiping a single continuous span.\\ \textbf{+2 Reach:} The effect is Lasting .}

\Spell{Psychic Genesis}{Mind $\bullet$$\bullet$$\bullet$$\bullet$$\bullet$}{Making}{Duration}{}{}{Academics, Expression, Science}%
{The mage creates a consciousness as a self-aware intelligence with a Twilight presence. The consciousness gains traits as a Rank 1 Goetia. The consciousness remains for the Duration of the spell as the mage’s loyal servant, and she is able to direct it to complete tasks without the use of any additional spells.\\ \textbf{+1 Reach:} The consciousness works as a Sleepwalker for purposes of assisting in ritual casting.\\ \textbf{+1 Reach:} For one Mana, the consciousness gains Rank 2.}

\Spell{Social Networking}{Mind $\bullet$$\bullet$$\bullet$$\bullet$$\bullet$}{Making}{Potency}{}{}{Persuasion, Politics, Socialize}%
{The mage creates social networks where none existed before. For each level of Potency, the subject gains one dot in one of the following Merits: Allies, Contacts, or Status.}


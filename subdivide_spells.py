#!/usr/bin/env python3

with open("spells.tex") as f:
    all_spells = f.read()

spells = all_spells.split("\n\n")[1:-1]

files = {}

for spell in spells:
    category = spell.split("{")[2].split("}")[0]
    arcanum = category.split(" ")[0]
    rank = category.count("bullet")
    #print("ARCANUM:",arcanum,rank,spell.split("}")[0].split("{")[1])
    if arcanum not in files:
        files[arcanum] = [""]*5

    files[arcanum][rank-1] += spell + "\n\n"

for a in files:
    for r in range(5):
        filename = a+"_"+str(r+1)+".tex"
        with open(filename, "w") as f:
            f.write(files[a][r])

#!/usr/bin/env python3

from extract import extract

spells = extract()

fields = ["practice", "primary_factor", "cost", "withstand", "suggested_rote_skills"]


tex = ""

tex += "%\\Spell{-name-}{-category-}"
for field in fields:
    tex += "{-"+field+"-}"
tex += "\n\n"

for arcanum in spells:
    for rank in spells[arcanum]:
        for spell in spells[arcanum][rank]:
            line = "\\Spell{"+spell["name"]+"}{"+arcanum+" "+spell["dots"]+"}"
            for field in fields:
                if field in spell:
                    line += "{"+spell[field]+"}"
                else:
                    line += "{}"
            line += "%\n"
            line += "{"+spell["description"]+"}"
            tex += line + "\n\n"

# A few little fixes
tex = tex.replace(". +", ".\\\\ \\textbf{+").replace("Reach: ", "Reach:} ")
tex = tex.replace(". Add ", ".\\\\ \\textbf{Add ").replace("•: ", "•:} ")
tex = tex.replace("$", "\$").replace("•", "$\\bullet$")

tex = tex.replace("\\textbf{+1 Reach ", "\\textbf{+1 Reach:} ")
tex = tex.replace("\\textbf{+2 Reach ", "\\textbf{+2 Reach:} ")

tex = tex.replace(" +1 Reach:}", "\\\\ \\textbf{+1 Reach:}")
tex = tex.replace(" +2 Reach:}", "\\\\ \\textbf{+2 Reach:}")





with open("spells.tex", "w") as f:
    f.write(tex)
            

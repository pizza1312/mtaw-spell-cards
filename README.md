# mtaw-spell-cards

PDF extraction and LaTeX typesetting for Mage: the Awakening spell cards.

The PDF extraction set can be automated like 90% of the way, but there's still a handful of inaccuracies in the content - 
you need to go over the .tex file (in Overleaf or sth) to make sure everything came out okay.

Check out the zip file for my final version.
